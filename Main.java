package com.mercadolibre;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class Main {

    public static class Pice {
        BufferedImage img;
        Integer upRight;
        Integer upLeft;
        Integer downRight;
        Integer downLeft;
    }

    public static void main(String[] args) throws IOException {

        // Parse

        File file = new File("/Users/egutierrez/Downloads/challenge.png");
        FileInputStream fis = new FileInputStream(file);
        BufferedImage image = ImageIO.read(fis);

        int rows = 20;
        int cols = 20;

        int chunkWidth = image.getWidth() / cols;
        int chunkHeight = image.getHeight() / rows;

        Pice imgs[][] = new Pice[cols][rows];

        Map<Integer, Integer> rgbCount = new HashMap<>();
        for (int x = 0; x < rows; x++) {
            for (int y = 0; y < cols; y++) {
                Pice pice = new Pice();
                pice.img = new BufferedImage(chunkWidth, chunkHeight, image.getType());

                draw(image, pice.img, chunkWidth, chunkHeight, x, y);
                pice.upLeft = getColor(chunkHeight - 5, 5, imgs);
                pice.upRight = getColor(chunkHeight - 5, chunkWidth - 5, imgs) ;
                pice.downLeft = getColor(5, 5, imgs);
                pice.downRight = getColor(chunkWidth - 5, 5, 5, 5, imgs);

                rgbCount.putIfAbsent(pice.upLeft, 0);
                rgbCount.putIfAbsent(pice.upRight, 0);
                rgbCount.putIfAbsent(pice.downLeft, 0);
                rgbCount.putIfAbsent(pice.downRight, 0);

                rgbCount.put(pice.upLeft, rgbCount.get(pice.upLeft) + 1);
                rgbCount.put(pice.upRight, rgbCount.get(pice.upRight) + 1);
                rgbCount.put(pice.downLeft, rgbCount.get(pice.downLeft) + 1);
                rgbCount.put(pice.downRight, rgbCount.get(pice.downRight) + 1);

                imgs[x][y] = pice;

            }
        }

        System.out.println(rgbCount);

        System.out.println("Splitting done");

        // Sort

        // Print

        BufferedImage result = new BufferedImage(image.getWidth(), image.getHeight(), image.getType());
        for (int x = 0; x < rows; x++) {
            for (int y = 0; y < cols; y++) {
                draw2(imgs[x][y].img, result, chunkWidth, chunkHeight, x, y);
            }
        }

        File outputfile = new File("/Users/egutierrez/Downloads/challenge_result.png");
        ImageIO.write(result, "png", outputfile);

    }
    
    private static int getColor(int x, int y, Pice imgs[][]) {
        if(Color.BLACK == pice.img.getRGB(x, y) || Color.WHITE == pice.img.getRGB(x, y)) {
            return pice.img.getRGB(x, y) 
        } else {
            return null;
        }
    }

    public static void draw2(BufferedImage image, BufferedImage chunk, int chunkWidth, int chunkHeight, int x, int y) throws IOException {
        Graphics2D gr = chunk.createGraphics();
        gr.drawImage(image,
                chunkWidth * y, chunkHeight * x,
                chunkWidth * y + chunkWidth, chunkHeight * x + chunkHeight,
                0, 0,
                chunkWidth, chunkHeight,
                null);
        gr.dispose();
    }

    public static void draw(BufferedImage image, BufferedImage chunk, int chunkWidth, int chunkHeight, int x, int y) throws IOException {
        Graphics2D gr = chunk.createGraphics();
        gr.drawImage(image,
                0, 0,
                chunkWidth, chunkHeight,
                chunkWidth * y, chunkHeight * x,
                chunkWidth * y + chunkWidth,
                chunkHeight * x + chunkHeight, null);
        gr.dispose();

        File outputfile = new File("/Users/egutierrez/Downloads/challenge_" + x + "_" + y + ".png");
        ImageIO.write(chunk, "png", outputfile);
    }

}
